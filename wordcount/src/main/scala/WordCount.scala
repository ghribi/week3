import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd._

object WordCount {
//------------------------------------------------------------------------------
    def main(args: Array[String]) = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)

        val spark = SparkSession.builder
                                 .appName("Word count").getOrCreate()
        val plots = loadData(spark)
        run(plots)
        spark.stop()
    }

//------------------------------------------------------------------------------
// Uncomment one of the MapReduce calls to use the combiner or not
//------------------------------------------------------------------------------
    def run(plots: RDD[String]) = {
        val mostFrequentWords = 
                mapReduce(plots, mapper, reducer) // comment to use combiner
                //mapReduce(plots, mapper, reducer, combiner) // and uncomment here
                    .sortBy(-_._2)
                    .take(100)
                    .mkString("\t")
        println(mostFrequentWords)
    }

//------------------------------------------------------------------------------
//  MapReduce
//------------------------------------------------------------------------------
    type WordCounts = Array[(String, Int)]
    type ShuffledWords = (String, Iterable[Int])
    def mapReduce(plots: RDD[String],
                  map: String => WordCounts, 
                  reduce: (Int, Int) => Int,
                  combine: WordCounts => WordCounts = identity):
                                                          RDD[(String, Int)] = {
        plots.flatMap(plot => combine(map(plot)))
             .reduceByKey(reduce)
    }

//------------------------------------------------------------------------------
    def mapper(plot: String): WordCounts = {
        val blacklisted = Set("about", "also", "after", "been", "does", "that", 
                              "from", "gets", "goes", "have", "into", "there", 
                              "they", "them", "their", "then", "this", "what", 
                              "when", "where", "which", "while", "will", "with")
        if (plot != null)
            plot.toLowerCase.split("\\W+")
                .filter(w => w.size > 3 && !blacklisted.contains(w))
                .map(w => (w, 1))
        else Array()
    }

//------------------------------------------------------------------------------
    def reducer(a: Int, b: Int) = a + b 

//------------------------------------------------------------------------------
//  Combiner
//------------------------------------------------------------------------------
    def combiner(localwords: WordCounts): WordCounts = {
        localwords.groupBy(_._1)
                  .mapValues(_.size)
                  .toArray
    }

//------------------------------------------------------------------------------
//  Auxiliary
//------------------------------------------------------------------------------
    def loadData(spark: SparkSession) = {
        import spark.implicits._
        val datafile = "/cs449/week3-exercises/wiki_movie_plots_deduped.csv"
        spark.read.options(Map("header" -> "true"))
             .csv(datafile).rdd
             .map(r => (r.getString(7)))
    }
//------------------------------------------------------------------------------
}
